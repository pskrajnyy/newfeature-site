import React, { Component } from "react";
import {Helmet} from 'react-helmet'

class PageHelmet extends Component{
    render(){
        return(
            <React.Fragment>
                <Helmet>
                    <title>{this.props.pageTitle} || Software House </title>
                    <meta name="description" content="NewFeature Software House site" />
                </Helmet>
            </React.Fragment>
        )
    }
}

export default PageHelmet;