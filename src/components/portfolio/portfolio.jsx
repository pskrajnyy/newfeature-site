import React, { Component } from "react";
import Slider from "react-slick";

export const portfolioSlick = {
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    dots: true,
    autoplay: false,
    arrows: false,
    responsive: [{
        breakpoint: 800,
        settings: {
            slidesToShow: 3,
        }
    },
    {
        breakpoint: 1200,
        settings: {
            slidesToShow: 3,
        }
    },
    {
        breakpoint: 993,
        settings: {
            slidesToShow: 2,
        }
    },
    {
        breakpoint: 769,
        settings: {
            slidesToShow: 2,
        }
    },
    {
        breakpoint: 481,
        settings: {
            slidesToShow: 1,
        }
    }
]
};

const PortfolioList = [
    {
        image: 'image-1',
        category: 'Strona wizytówka',
        title: 'Przemysław Skrajny',
        link: 'https://przemyslawskrajny.pl'
    },
    {
        image: 'image-2',
        category: 'Strona wizytówka',
        title: 'New Feature - Software House',
        link: 'http://newfeature.pl'
    },
    {
        image: 'image-3',
        category: 'Strona wizytówka',
        title: 'Admin panel',
        link: 'https://pskrajnyy.gitlab.io/just-do-it-project/'
    },
    {
        image: 'image-4',
        category: 'Strona wizytówka',
        title: 'Bazar Shop',
        link: 'https://affectionate-chandrasekhar-e4e135.netlify.app/#'
    },
    {
        image: 'image-5',
        category: 'Strona wizytówka',
        title: 'Harbinger Shop',
        link: 'https://pskrajnyy.gitlab.io/example_page_bootstrap/'
    },
    {
        image: 'image-6',
        category: 'Strona wizytówka',
        title: 'Travel Agency',
        link: 'https://travel-react.herokuapp.com/trips'
    }
]

class Portfolio extends Component{
    render(){
        let title = 'Nasze realizacje',
        description = 'Projekty zawsze są dla nas nowym wyzwaniem. W naszym portfolio znajdują się projekty różnego rodzaju grafik, kompleksowego outsourcingu całości developmentu, a także zlecenia dotyczące stron wizytówek, sklepów internetowych czy aktualizowanych serwisów www.';
        return(
            <React.Fragment>
                <div className="portfolio-wrapper">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9">
                                <div className="section-title">
                                    <h2>{title}</h2>
                                    <p>{description}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="portfolio-slick-activation mt--70 mt_sm--40">
                        <Slider {...portfolioSlick}>
                            {PortfolioList.map((value , index) => (
                                <div className="portfolio" key={index}>
                                    <div className="thumbnail-inner">
                                        <div className={`thumbnail ${value.image}`}></div>
                                        <div className={`bg-blr-image ${value.image}`}></div>
                                    </div>
                                    <div className="content">
                                        <div className="inner">
                                            <p>{value.category}</p>
                                            <h4>{value.title}</h4>
                                            <div className="portfolio-button">
                                                <a className="rn-btn" href={value.link}>Zobacz</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </Slider>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default Portfolio;