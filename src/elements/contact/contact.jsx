import React, { Component } from "react";
import { FiHeadphones, FiMail } from "react-icons/fi";
import {FaMapMarkedAlt} from 'react-icons/fa';
import * as emailjs from 'emailjs-com';
import autosize from "autosize";

class Contact extends Component{
    constructor(props){
        super(props);
        this.state = {
            rnName: '',
            rnEmail: '',
            rnSubject: '',
            rnPhone: '',
            rnMoney: '',
            rnMessage: '',            
            error: false,
        };
    }

    componentDidMount() {
        this.textarea.focus();
        autosize(this.textarea);
      }

    formSubmit() {
        if (this.state.rnName === '' || this.state.rnEmail === '' || this.state.rnSubject === '' || this.state.rnPhone === '' || this.state.rnMessage === '') {
          this.setState({error: true});
        } else {
            this.setState({error: false});
            const {rnName, rnEmail, rnSubject, rnPhone, rnMoney, rnMessage} = this.state;
    
            let templateParams = {
                from_name: rnName,
                from_email: rnEmail,
                from_topic: rnSubject,
                from_phone: rnPhone,
                from_money: rnMoney,
                to_name: 'newFeature',
                message_html: rnMessage,
            }
            
            emailjs.send(
                'newFeature',
                'newfeature',
                templateParams,
                'user_fXBSTEmjB4HVsjNa5ggFx'
            )
         
            this.resetForm();
        }
        this.forceUpdate();
    }

    resetForm() {
        this.setState({ rnName: '', rnEmail: '', rnSubject: '', rnPhone: '', rnMoney: '', rnMessage: ''})
        document.getElementById('item06').value = '';
    }

    check(val) {
        if (this.state.error && val === '') {
            return false;
        } else {
            return true;
        }
    }

    render(){
        return(
            <div className="contact-form--1">
                <div className="container">
                    <div className="row row--35 align-items-start">
                        <div className="col-lg-6 order-1 order-lg-1">
                            <div className="section-title text-left mb--50">
                                <h2 className="title">Skontaktuj się.</h2>
                                <p className="description">Napisz do nas aby otrzymać indywidualna wycenę dla twojego projektu!</p>
                            </div>
                            <div className="form-wrapper">
                                <form>
                                    <label htmlFor="item01">
                                        <input
                                            type="text"
                                            name="name"
                                            id="item01"
                                            value={this.state.rnName}
                                            onChange={e => this.setState({rnName: e.target.value})}
                                            placeholder="Imię i nazwisko*"
                                            className={`${this.check(this.state.rnName) ? '' : 'errorInput'}`}
                                        />
                                    </label>

                                    <label htmlFor="item02">
                                        <input
                                            type="text"
                                            name="email"
                                            id="item02"
                                            value={this.state.rnEmail}
                                            onChange={(e)=>{this.setState({rnEmail: e.target.value});}}
                                            placeholder="Email*"
                                            className={`${this.check(this.state.rnEmail) ? '' : 'errorInput'}`}
                                        />
                                    </label>

                                    <label htmlFor="item03">
                                        <input
                                            type="text"
                                            name="subject"
                                            id="item03"
                                            value={this.state.rnSubject}
                                            onChange={(e)=>{this.setState({rnSubject: e.target.value});}}
                                            placeholder="Temat*"
                                            className={`${this.check(this.state.rnSubject) ? '' : 'errorInput'}`}
                                        />
                                    </label>
                                    <label htmlFor="item04">
                                        <input
                                            type="number"
                                            name="phone"
                                            id="item04"
                                            value={this.state.rnPhone}
                                            onChange={(e)=>{this.setState({rnPhone: e.target.value});}}
                                            keyboardType='numeric'
                                            placeholder="Numer telefonu*"
                                            className={`${this.check(this.state.rnPhone) ? '' : 'errorInput'}`}
                                        />
                                    </label>
                                    <label htmlFor="item05">
                                        <input
                                            type="number"
                                            name="money"
                                            id="item05"
                                            value={this.state.rnMoney}
                                            onChange={(e)=>{this.setState({rnMoney: e.target.value});}}
                                            placeholder="Przewidywany budzet w PLN"
                                        />
                                    </label>
                                    <label htmlFor="item06">
                                        <textarea
                                            style={{overflow:'auto', resize: 'none'}}
                                            type="text"
                                            id="item06"
                                            ref={c => (this.textarea = c)}
                                            name="message"
                                            value={this.state.rnMessage}
                                            onChange={(e)=>{this.setState({rnMessage: e.target.value});}}
                                            placeholder="Wiadomość*"
                                            className={`${this.check(this.state.rnMessage) ? '' : 'errorInput'}`}
                                        />
                                    </label>
                                    <button className="rn-button-style--2 btn-solid" type="button" onClick={() => this.formSubmit()} id="mc-embedded-subscribe">Wyślij</button>
                                </form>
                            </div>
                        </div>

                        <div id="contact-icons" className="col-lg-6 order-2 order-lg-1">
                            <div className="col-lg-12">
                                <div className="rn-address">
                                    <div className="icon">
                                        <FiHeadphones />
                                    </div>
                                    <div className="inner">
                                        <h4 className="title">Zadzwoń:</h4>
                                        <p><a href="tel:660247705">Przemek: 660-247-705</a></p>
                                    </div>
                                </div>
                            </div>

                            <div className="col-lg-12">
                                <div className="rn-address">
                                    <div className="icon">
                                        <FiMail />
                                    </div>
                                    <div className="inner">
                                    <h4 className="title">Napisz na e-mail:</h4>
                                    <p><a href="mailto:kontakt@newfeature.pl">kontakt@newfeature.pl</a></p>
                                </div>
                                </div>
                            </div>

                            <div className="col-lg-12">
                                <div className="rn-address">
                                    <div className="icon">
                                        <FaMapMarkedAlt />
                                    </div>
                                    <div className="inner">
                                    <h4 className="title">Dane:</h4>
                                    <p>Przemysław Skrajny</p>
                                    <p>Morska 220</p>
                                    <p>81-006 Gdynia</p>
                                    <p>NIP: 9581706142</p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Contact;