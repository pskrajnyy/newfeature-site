import React, { Component } from "react";
import { FiShoppingBag , FiSmartphone, FiMonitor, FiCode, FiSettings, FiAperture } from "react-icons/fi";

const ServiceList = [
    {
        icon: <FiMonitor />,
        title: 'Strony Internetowe',
        description: 'Nie tylko zbudujemy dla Ciebie profesjonalne, responsywne i intuicyjne strony internetowe, ale i przebudujemy te już istniejące, aby spełniały obecne wymogi techniczne. Witryna dopasowana do potrzeb Twoich i Twoich klientów pozwoli Ci stworzyć silną pozycję Twojej marki w internecie.',
        link: '/strony-internetowe'
    },
    {
        icon: <FiCode />,
        title: 'Aplikacje internetowe',
        description: 'Tworzymy nowoczesne aplikacje webowe od fazy pomysłu i projektu po wdrażanie i testowanie. Stawiamy na innowacyjne funkcjonalności i wysoką intuicyjność naszych realizacji, które ułatwią użytkownikom poruszanie się po aplikacji, a Tobie pozwolą osiągnąć zamierzone cele biznesowe.',
        link: '/aplikacje-webowe'
    },
    {
        icon: <FiShoppingBag />,
        title: 'Sklepy Internetowe',
        description: 'Naszą misją jest tworzenie nowoczesnych sklepów internetowych, które pozwolą Ci osiągnąć założone cele biznesowe i wyróżnić się na tle konkurencji. Starannie przemyślana szata graficzna i intuicyjna budowa platformy e-commerce, umożliwi Twoim klientom szybki zakup bez porzucania koszyka.',
        link: '/sklepy-internetowe'
    }, 
    {
        icon: <FiSettings />,
        title: 'Integracje aplikacji i systemów API',
        description: 'Tworzymy platformy integracyjne, które pozwalają łączyć wiele systemów w jeden sprawnie działający organizm.',
        link: '/aplikacje-mobilne'
    },   
    {
        icon: <FiSmartphone />,
        title: 'Aplikacje mobilne',
        description: 'Z nami zaprojektujesz od podstaw swoją aplikację mobilną. Razem przejdziemy przez proces projektowania, implementacji oraz wdrażania. Oferujemy również utrzymanie obecnie istniejących aplikacji.',
        link: '/aplikacje-mobilne'
    },
    {
        icon: <FiAperture />,
        title: 'Utrzymanie istniejących systemów',
        description: 'Bieżąca aktualizacje tworzonych stron internetowych, aplikacji czy też sklepów internetowych, jak również wdrażanie oprogramowania CMS do istniejących systemów.',
        link: '/aplikacje-mobilne'
    }
]

class Service extends Component{
    render(){
        return(
            <React.Fragment>
                <div className="row">                    
                    <div>
                        <div className="section-title text-left mb--30">
                            <h2>Nasza oferta</h2>
                            <p>Wiemy, jak cenić Twój czas, dlatego oferujemy Twojej firmie kompleksowe usługi na najwyższym poziomie. Strony internetowe, aplikacje webowe, sklepy e-commerce, aplikacje webowe i portale internetowe to obszary, w których się specjalizujemy. Przygotowaliśmy dla Ciebie całościową ofertę – od pomysłu po projekty graficzne, aż po gotowy produkt.</p>
                            <div className="service-btn">
                                <a className="btn-transparent rn-btn-dark" href="#contact"><span className="text">Skontaktuj się z nami</span></a>
                            </div>
                        </div>
                    </div>                    
                    <div className="col-lg-12 col-12 mt_md--50">
                        <div className="row service-one-wrapper">
                            {ServiceList.map( (val , i) => (
                                <div className="col-xl-4 col-lg-6 col-12" key={i}>
                                    {/* <a href={val.link}> */}
                                        <div className="service service__style--2">
                                            <div className="icon">
                                                {val.icon}
                                            </div>
                                            <div className="content">
                                                <h3 className="title">{val.title}</h3>
                                                <p>{val.description}</p>
                                            </div>
                                        </div> 
                                    {/* </a>                                                                        */}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default Service;
