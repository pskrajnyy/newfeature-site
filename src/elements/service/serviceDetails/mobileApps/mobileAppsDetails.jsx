import React, { Component } from "react";
import PageHelmet from '../../../../components/helmet/helmet';
import Footer from '../../../../components/footer/footer';
import Scrollspy from 'react-scrollspy';
import ScrollToTop from 'react-scroll-up';
import { FiChevronUp, FiX , FiMenu } from "react-icons/fi";

class MobileAppsDetails extends Component{
    constructor () {
        super()
        this.state = {
          isOpen: false
        }
        this.openModal = this.openModal.bind(this)
    }

    openModal () {
        this.setState({isOpen: true})
    }

    render(){
        return(
            <React.Fragment>
                
                {/* Start Pagehelmet  */}
                <PageHelmet pageTitle='Service Details' />
                {/* End Pagehelmet  */}

                {/* Start Header Area   */}
                <header className="header-area formobile-menu header--fixed default-color">
                    <div className="header-wrapper" id="header-wrapper">
                        <div className="header-left">
                            <div className="logo">
                                <a href="/">
                                    <img className="logo-1" src="/assets/images/logo/logo.png" alt="Logo Images"/>
                                </a>
                            </div>
                        </div>
                        <div className="header-right">
                            <nav className="mainmenunav d-lg-block">
                                <Scrollspy className="mainmenu" items={['home','service', 'stack', 'about', 'portfolio', 'contact']} currentClassName="is-current" offset={-200}>
                                    <li><a href="/">Home</a></li>
                                    <li><a href="/#service">Oferta</a></li>
                                    <li><a href="/#stack">Narzędzia</a></li>
                                    <li><a href="/#about">O nas</a></li>
                                    <li><a href="/#portfolio">Portfolio</a></li>
                                    {/* <li><a href="#team">Zespół</a></li> */}
                                    <li><a href="/#contact">Kontakt</a></li>
                                </Scrollspy>
                            </nav>
                            {/* Start Humberger Menu  */}
                            <div className="humberger-menu d-block d-lg-none pl--20">
                                <span onClick={this.menuTrigger} className="menutrigger text-white"><FiMenu /></span>
                            </div>
                            {/* End Humberger Menu  */}
                            <div className="close-menu d-block d-lg-none">
                                <span onClick={this.CLoseMenuTrigger} className="closeTrigger"><FiX /></span>
                            </div>
                        </div>
                    </div>
                </header>
                {/* End Header Area   */}

                {/* Start Breadcrump Area */}
                <div className="rn-page-title-area ptb--50 bg_image bg_image--5"  data-black-overlay="5">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="rn-page-title text-center pt--100">
                                    <h2 className="title theme-gradient">Aplikacje mobilne</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Breadcrump Area */}

                {/* Start Page Wrapper */}
                <div className="rn-service-details ptb--120 bg_color--1">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="service-details-inner">
                                    <div className="inner">
                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Zapytanie ofertowe</h4>
                                                    <p>Prześlij do nas informacje o planowanym projekcie korzystając z formularza kontaktu na dole strony albo wyślij maila lub zadzwoń bezpośrednio do naszego konsultanta. Skontaktujemy się z tobą możliwie szybko w celu doprecyzowania wymagań. Możemy również podpisać na wstępie umowę o zachowaniu poufności oraz spotkać się w Tarnowie lub Krakowie.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Wycena i umowa</h4>
                                                    <p>Po zebraniu wszystkich potrzebnych informacji przygotujemy dla ciebie ofertę z wyceną. Jeśli przedstawiona wycena zostanie zaakceptowana to kolejnym krokiem będzie podpisanie umowy, która precyzuje najważniejsze kwestie dotyczące przebiegu projektu: zakres prac, wynagrodzenie, harmonogram, warunki odbioru, gwarancję, licencje i prawa autorskie, przekazanie kodów źródłowych.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Mockupy ekranów i projekt techniczny</h4>
                                                    <p>ace nad projektem zaczynamy od narysowania mockupów wszystkich ekranów aplikacji. Zaakceptowane przez zleceniodawcę mockupy ekranów trafiają do grafika oraz programistów. Równolegle z mockupami lub bezpośrednio po ich stworzeniu powstaje projekt techniczny systemu (specyfikacja API, schemat bazy danych, diagramy, itp).</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Projekt graficzny</h4>
                                                    <p>Kolejnym etapem jest stworzenie przez grafika docelowego projektu graficznego aplikacji na bazie przygotowanych wcześniej mockupów oraz wytycznych i materiałów otrzymanych od zleceniodawcy (logotyp, kolorystyka, itp). Projekt graficzny jest przesyłany zleceniodawcy do akceptacji.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Implementacja w metodyce Scrum</h4>
                                                    <p>Prace programistyczne są realizowane w jedno lub dwutygodniowych sprintach. Aplikacje mobilne są pisane natywnie tj. w języku programowania specyficznym dla danej platformy, dzięki czemu są zawsze responsywne, zgodne z wzorcami projektowymi i dostosowane do najnowszych wersji systemów operacyjnych. Za stworzenie aplikacji na każdą platformę odpowiada inny programista.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Testy i akceptacja</h4>
                                                    <p>Aplikacja przechodzi szczegółowe testy wewnętrzne według przygotowanych wcześniej scenariuszy testowych. Testy są wykonywane na wielu urządzeniach mobilnych różniących się parametrami technicznymi. Następnie wersja beta jest udostępniana zleceniodawcy do testów i akceptacji. Wszystkie poprawki i uwagi są na bieżąco wprowadzane w aplikacji aż do momentu uzyskania wersji, która zostanie zaakceptowana przez zleceniodawcę.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Publikacja aplikacji w marketach</h4>
                                                    <p>Zaakceptowana aplikacja jest publikowana w marketach z konta zleceniodawcy lub z konta ALTCONNECT (aplikacje płatne lub zawierające płatny kontent muszą być publikowane z konta zleceniodawcy). Pomagamy w przejściu przez proces rejestracji w programach deweloperskich Apple i Google.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Hosting i wsparcie techniczne (SLA)</h4>
                                                    <p>Backend aplikacji mobilnych (API, CMS) oraz aplikacje webowe są instalowane na serwerach w chmurze. Usługa wsparcia technicznego obejmuje m.in.: administrację serwerami, monitoring dostępności usługi i parametrów systemu, analizę logów, raportowanie „crash’y” aplikacji, automatyczne backupy danych, przywracanie danych z backupu, diagnozowanie i naprawę błędów.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Marketing aplikacji mobilnych</h4>
                                                    <p>Publikacja aplikacji w marketach to dopiero początek drogi do sukcesu. Nasza ofertamarketingu aplikacji mobilnych obejmuje m.in.: stworzenie landing page i explainer video,stworzenie tutorialu aplikacji, konfigurację narzędzi analitycznych, prowadzenie kampaniiAdWords, prowadzenie kampanii na Facebooku, App Store Optimization (ASO).</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Page Wrapper */}
                
                {/* Start Back To Top */}
                <div className="backto-top">
                    <ScrollToTop showUnder={160}>
                        <FiChevronUp />
                    </ScrollToTop>
                </div>
                {/* End Back To Top */}

                {/* Start Footer Style   */}
                <Footer />
                {/* End Footer Style  */}

            </React.Fragment>
        )
    }
}
export default MobileAppsDetails;