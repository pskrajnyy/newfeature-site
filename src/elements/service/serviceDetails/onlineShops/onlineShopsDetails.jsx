import React, { Component } from "react";
import PageHelmet from '../../../../components/helmet/helmet';
import Footer from '../../../../components/footer/footer';
import Scrollspy from 'react-scrollspy';
import ScrollToTop from 'react-scroll-up';
import { FiChevronUp, FiX , FiMenu } from "react-icons/fi";

class OnlineShopsDetails extends Component{
    constructor () {
        super()
        this.state = {
          isOpen: false
        }
        this.openModal = this.openModal.bind(this)
    }

    openModal () {
        this.setState({isOpen: true})
    }

    render(){
        return(
            <React.Fragment>
                
                {/* Start Pagehelmet  */}
                <PageHelmet pageTitle='Service Details' />
                {/* End Pagehelmet  */}

                {/* Start Header Area   */}
                <header className="header-area formobile-menu header--fixed default-color">
                    <div className="header-wrapper" id="header-wrapper">
                        <div className="header-left">
                            <div className="logo">
                                <a href="/">
                                    <img className="logo-1" src="/assets/images/logo/logo.png" alt="Logo Images"/>
                                </a>
                            </div>
                        </div>
                        <div className="header-right">
                            <nav className="mainmenunav d-lg-block">
                                <Scrollspy className="mainmenu" items={['home','service', 'stack', 'about', 'portfolio', 'contact']} currentClassName="is-current" offset={-200}>
                                    <li><a href="/">Home</a></li>
                                    <li><a href="/#service">Oferta</a></li>
                                    <li><a href="/#stack">Narzędzia</a></li>
                                    <li><a href="/#about">O nas</a></li>
                                    <li><a href="/#portfolio">Portfolio</a></li>
                                    {/* <li><a href="#team">Zespół</a></li> */}
                                    <li><a href="/#contact">Kontakt</a></li>
                                </Scrollspy>
                            </nav>
                            {/* Start Humberger Menu  */}
                            <div className="humberger-menu d-block d-lg-none pl--20">
                                <span onClick={this.menuTrigger} className="menutrigger text-white"><FiMenu /></span>
                            </div>
                            {/* End Humberger Menu  */}
                            <div className="close-menu d-block d-lg-none">
                                <span onClick={this.CLoseMenuTrigger} className="closeTrigger"><FiX /></span>
                            </div>
                        </div>
                    </div>
                </header>
                {/* End Header Area   */}

                {/* Start Breadcrump Area */}
                <div className="rn-page-title-area ptb--50 bg_image bg_image--5"  data-black-overlay="5">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="rn-page-title text-center pt--100">
                                    <h2 className="title theme-gradient">Sklepy internetowe</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Breadcrump Area */}

                {/* Start Page Wrapper */}
                <div className="rn-service-details ptb--120 bg_color--1">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="service-details-inner">
                                    <div className="inner">
                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Skuteczny sklep internetowy</h4>
                                                    <p>Skuteczny sklep internetowy to sklep, który wyróżni się na rynku wśród silnej konkurencji. O jego skuteczności decyduje wiele czynników. Bez względu na oferowane przez niego usługi lub produkty, to na jego wygląd w pierwszym rzędzie zwróci uwagę klient. Sama szata graficzna sklepu online powinna być zatem maksymalnie estetyczna i przejrzysta. Z kolei wszystkie obecne na nim zdjęcia powinny cechować się najwyższą jakością. Zaprojektujemy dla Ciebie wyjątkowy i niepowtarzalny wygląd Twojego sklepu e-commerce, co sprawi, że Twoja marka nie będzie mylona z konkurencją.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Profesjonalne sklepy internetowe</h4>
                                                    <p>Profesjonalny sklep internetowy pozwoli Twoim klientom w łatwy i szybki sposób odnaleźć interesujące ich produkty i usługi. Zbudujemy dla Ciebie platformę sprzedażową z wykorzystaniem najnowszych technologii zgodnie z zasadami Google. Wyposażymy ją we wszelkie niezbędne integracje i dodatki, które pomogą Ci samodzielnie nią zarządzać i wyposażyć w coraz to nowe możliwości przyciągające klientów. Oddamy w Twoje ręce całkowicie responsywny sklep internetowy. Dzięki temu Twoi klienci będą mogli dokonywać zakupu w drodze do pracy czy szkoły.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Sklepy internetowe przynoszące zysk</h4>
                                                    <p>Dobry sklep internetowy to przede wszystkim sklep, który generuje coraz większe zyski. Zbudujemy przyjazną strukturę informacji na Twojej platformie sprzedażowej, aby Twoi klienci z łatwością dotarli do interesujących ich produktów, usług oraz informacji. Dzięki temu samo kupowanie będzie dla nich prawdziwą przyjemnością. Podpowiemy Ci, jakie rozwiązania są najlepsze dla Twojego e-biznesu. Doradzimy, które integracje i działania marketingowe znacząco wpłyną na płynność zakupów i zwiększą zainteresowanie Twoją ofertą, abyś mógł zarabiać coraz więcej.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Funkcjonalne sklepy internetowe</h4>
                                                    <p>Aby wyróżnić swój e-biznes spośród licznej konkurencji i zbudować jego silną pozycję w sieci, musi on wychodzić naprzeciw oczekiwaniom klientów. Dotyczy to nie tylko jakości usług i produktów. Funkcjonalny sklep internetowy musi posiadać szereg udogodnień, także dla Ciebie. Od automatycznych modułów płatności, wyboru sposobu płatności, systemów kuponów i rabatów po integracje z zewnętrznymi systemami np.: CRM lub ERP. Tworząc funkcjonalny sklep e-commerce dla rozwoju Twojej firmy wyposażymy go w funkcjonalności zarówno te podstawowe i absolutnie konieczne, po najnowsze technologie. Ułatwią one użytkownikom dokonywanie zakupów, a Tobie sprawowanie kontroli nad wzrastającą sprzedażą.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Szybkie sklepy internetowe</h4>
                                                    <p>Platformy sprzedażowe oferują setki, a nawet tysiące produktów, przez co przepełnione są treściami i zdjęciami. Przeładowane nieskompresowanymi grafikami sklepy ładują się bardzo wolno, dlatego klienci zwyczajnie je opuszczają i już nigdy na nie wracają. Szybki sklep internetowy bazuje na zoptymalizowanych zdjęciach w dobrej jakości i rozdzielczości oraz na uporządkowanym kodzie, dzięki czemu witryna nie jest nadmiernie obciążona. Każdy z nas ceni sobie szybkie i bezproblemowe zakupy. Dlatego też wyposażymy Twój sklep e-commerce m.in.: w bezpieczne płatności online, szybkie logowanie się przez social media, porównywarkę produktów, obsługę wielu walut czy też możliwość dokonywania zakupów bez rejestracji. Dzięki temu zakupy na Twojej platformie sprzedażowej będą jeszcze szybsze i przyjemniejsze.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Sklepy internetowe dostosowane do wymagań SEO</h4>
                                                    <p>Bez względu na rodzaj branży, w której prowadzisz e-biznes konkurencja, z którą musisz się zmierzyć jest ogromna. Dlatego też pozycjonowanie sklepu internetowego jest działaniem całkowicie niezbędnym. Bez optymalizacji SEO praktycznie nie ma mowy, aby mógł on generować zysk i budować silną pozycję Twojej marki w sieci. Jako agencja interaktywna wesprzemy Cię w dobraniu odpowiednich słów kluczowych produktów i kategorii, stworzymy odpowiednią strukturę nawigacji i przyjazne adresy URL oraz odpowiednie linkowanie pomiędzy stronami, kategoriami i produktami i wiele innych. Wszystko po to, aby Twój sklep internetowy osiągnął wysokie pozycje w naturalnych (organicznych) wynikach wyszukiwania.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Page Wrapper */}
                
                {/* Start Back To Top */}
                <div className="backto-top">
                    <ScrollToTop showUnder={160}>
                        <FiChevronUp />
                    </ScrollToTop>
                </div>
                {/* End Back To Top */}

                {/* Start Footer Style   */}
                <Footer />
                {/* End Footer Style  */}

            </React.Fragment>
        )
    }
}
export default OnlineShopsDetails;