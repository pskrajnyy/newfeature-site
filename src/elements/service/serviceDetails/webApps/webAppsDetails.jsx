import React, { Component } from "react";
import PageHelmet from '../../../../components/helmet/helmet';
import Footer from '../../../../components/footer/footer';
import Scrollspy from 'react-scrollspy';
import ScrollToTop from 'react-scroll-up';
import { FiChevronUp, FiX , FiMenu } from "react-icons/fi";

class WebAppsDetails extends Component{
    constructor () {
        super()
        this.state = {
          isOpen: false
        }
        this.openModal = this.openModal.bind(this)
    }

    openModal () {
        this.setState({isOpen: true})
    }

    render(){
        return(
            <React.Fragment>
                
                {/* Start Pagehelmet  */}
                <PageHelmet pageTitle='Service Details' />
                {/* End Pagehelmet  */}

                {/* Start Header Area   */}
                <header className="header-area formobile-menu header--fixed default-color">
                    <div className="header-wrapper" id="header-wrapper">
                        <div className="header-left">
                            <div className="logo">
                                <a href="/">
                                    <img className="logo-1" src="/assets/images/logo/logo.png" alt="Logo Images"/>
                                </a>
                            </div>
                        </div>
                        <div className="header-right">
                            <nav className="mainmenunav d-lg-block">
                                <Scrollspy className="mainmenu" items={['home','service', 'stack', 'about', 'portfolio', 'contact']} currentClassName="is-current" offset={-200}>
                                    <li><a href="/">Home</a></li>
                                    <li><a href="/#service">Oferta</a></li>
                                    <li><a href="/#stack">Narzędzia</a></li>
                                    <li><a href="/#about">O nas</a></li>
                                    <li><a href="/#portfolio">Portfolio</a></li>
                                    {/* <li><a href="#team">Zespół</a></li> */}
                                    <li><a href="/#contact">Kontakt</a></li>
                                </Scrollspy>
                            </nav>
                            {/* Start Humberger Menu  */}
                            <div className="humberger-menu d-block d-lg-none pl--20">
                                <span onClick={this.menuTrigger} className="menutrigger text-white"><FiMenu /></span>
                            </div>
                            {/* End Humberger Menu  */}
                            <div className="close-menu d-block d-lg-none">
                                <span onClick={this.CLoseMenuTrigger} className="closeTrigger"><FiX /></span>
                            </div>
                        </div>
                    </div>
                </header>
                {/* End Header Area   */}

                {/* Start Breadcrump Area */}
                <div className="rn-page-title-area ptb--50 bg_image bg_image--5"  data-black-overlay="5">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="rn-page-title text-center pt--100">
                                    <h2 className="title theme-gradient">Aplikacje webowe</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Breadcrump Area */}

                {/* Start Page Wrapper */}
                <div className="rn-service-details ptb--120 bg_color--1">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="service-details-inner">
                                    <div className="inner">
                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Aplikacje webowe szyte na miarę</h4>
                                                    <p>Nasze aplikacje webowe projektujemy w oparciu o Twój pomysł całkowicie od podstaw. Zarówno projekt szaty graficznej, jak i dobór odpowiednich rozwiązań technologicznych dostosujemy do Twoich indywidualnych oczekiwań. Weźmiemy także pod uwagę jej przyszły rozwój. Dlatego też nasze realizacje są oryginalne i w pełni dostosowane do założeń i celów, jakie ma spełniać przyszły produkt.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Tworzenie aplikacji webowych w oparciu o analizę potrzeb</h4>
                                                    <p>Przed przystąpieniem do projektu przeprowadzimy z Tobą szczegółowy wywiad dotyczących Twoich oczekiwań oraz celów, które chcesz spełniać za pomocą aplikacji. Wesprzemy Cię naszą radą i spostrzeżeniami, uczciwie doradzimy lub odradzimy konkretne rozwiązania technologiczne, mając na celu skuteczność Twojego systemu. Przeprowadzimy również research docelowej grupy użytkowników, tak aby nasze aplikacje webowe docierały do jak największego grona użytkowników i skutecznie na nich oddziaływały.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Specyfikacja aplikacji webowej</h4>
                                                    <p>Tworząc aplikację webową przygotujemy dla Ciebie także jej specyfikację. W naszej dokumentacji odnajdziesz wszelkie informacje dotyczące tematów technicznych i funkcjonalnych systemu wraz ze szczegółowym opisem ich zastosowania. Dzięki specyfikacji aplikacji webowej wspólnie ustalimy potrzebę zastosowania określonych technologii, zaplanujemy dalszy rozwój systemu oraz dokładnie oszacujemy koszty jej realizacji.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Projekt graficzny aplikacji webowych</h4>
                                                    <p>Przygotujemy dla Ciebie indywidualny projekt graficzny aplikacji webowej w oparciu o najnowsze trendy designerskie. Na początku przedstawimy Ci makiety na najnowszych narzędziach deweloperskich, na których będziesz mógł zobaczyć je „na żywo”. Nasze projekty graficzne są oryginalne i niepowtarzalne, dzięki czemu skutecznie wyróżnią się na tle konkurencji i przykują uwagę użytkownika. A dodatkowo korzystanie z nich jest wyjątkowo przyjemne i budzi dobre skojarzenia.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Projekt funkcjonalny aplikacji webowej</h4>
                                                    <p>Bez względu na to, jak bardzo skomplikowana jest budowa aplikacji webowej, zanim przystąpimy do procesu jej realizacji opracujemy jej projekt funkcjonalny. Pozwoli to określić warunki wzajemnej współpracy nad nią. Dokument ten stanowi swojego rodzaju przewodnik, zawierający wytyczne dotyczące każdego etapu pracy. Projekt funkcjonalny aplikacji internetowej bez problemu będziemy mogli modyfikować w zależności od nowo powstałych pomysłów. Sam dokument może również posłużyć jako wzór do tworzenia kolejnych aplikacji webowych.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Wdrożenie, testy i uruchomienie aplikacji webowej</h4>
                                                    <p>Po akceptacji projektu graficznego przejdziemy do wdrożenia aplikacji webowej. Wszystkie dotychczasowe prace przełożymy na język nowoczesnych technologii programistycznych. Rozpoczniemy także fazę jej testowania, w której będziesz mógł aktywnie uczestniczyć. Pozwoli to nam wychwycić wszelkie błędy i nieprawidłowości w funkcjonowaniu aplikacji webowej, a czasem zastąpić jedne rozwiązaniami innymi, lepszymi dla dalszego jej rozwoju. Nasze aplikacje webowe testujemy także po ich uruchomieniu, wszystko po to, aby jak najlepiej spełniały swoje zadania i Twoje oczekiwania.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Funkcjonalne aplikacje webowe</h4>
                                                    <p>Projektujemy i wdrażamy funkcjonalne aplikacje webowe, które zachwycają swoją kreatywnością i będą użyteczne dla Twoich klientów. Nasze projekty realizujemy zgodnie z zasadami dotyczącymi User Experience i User Interface, dzięki czemu zawierają szereg nowoczesnych funkcjonalności, logiczną i intuicyjny strukturę, która pozwala użytkownikom na szybką i łatwą obsługę aplikacji dedykowanej. Zwiększa to poziom interakcji systemu z klientem, a co za tym idzie przynosi oczekiwane korzyści Twojemu biznesowi. Wykorzystujemy bardzo zaawansowane technologie nastawione na bezproblemową komunikację z serwerem i zewnętrznymi integracjami API m.in.: płatności online, hurtownie danych czy systemy bilingowe.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Page Wrapper */}
                
                {/* Start Back To Top */}
                <div className="backto-top">
                    <ScrollToTop showUnder={160}>
                        <FiChevronUp />
                    </ScrollToTop>
                </div>
                {/* End Back To Top */}

                {/* Start Footer Style   */}
                <Footer />
                {/* End Footer Style  */}

            </React.Fragment>
        )
    }
}
export default WebAppsDetails;