import React, { Component } from "react";
import PageHelmet from '../../../../components/helmet/helmet';
import Footer from '../../../../components/footer/footer';
import Scrollspy from 'react-scrollspy';
import ScrollToTop from 'react-scroll-up';
import { FiChevronUp, FiX , FiMenu } from "react-icons/fi";

class WebPagesDetails extends Component{
    constructor () {
        super()
        this.state = {
          isOpen: false
        }
        this.openModal = this.openModal.bind(this)
    }

    openModal () {
        this.setState({isOpen: true})
    }

    render(){
        return(
            <React.Fragment>
                
                {/* Start Pagehelmet  */}
                <PageHelmet pageTitle='Service Details' />
                {/* End Pagehelmet  */}

                {/* Start Header Area   */}
                <header className="header-area formobile-menu header--fixed default-color">
                    <div className="header-wrapper" id="header-wrapper">
                        <div className="header-left">
                            <div className="logo">
                                <a href="/">
                                    <img className="logo-1" src="/assets/images/logo/logo.png" alt="Logo Images"/>
                                </a>
                            </div>
                        </div>
                        <div className="header-right">
                            <nav className="mainmenunav d-lg-block">
                                <Scrollspy className="mainmenu" items={['home','service', 'stack', 'about', 'portfolio', 'contact']} currentClassName="is-current" offset={-200}>
                                    <li><a href="/">Home</a></li>
                                    <li><a href="/#service">Oferta</a></li>
                                    <li><a href="/#stack">Narzędzia</a></li>
                                    <li><a href="/#about">O nas</a></li>
                                    <li><a href="/#portfolio">Portfolio</a></li>
                                    {/* <li><a href="#team">Zespół</a></li> */}
                                    <li><a href="/#contact">Kontakt</a></li>
                                </Scrollspy>
                            </nav>
                            {/* Start Humberger Menu  */}
                            <div className="humberger-menu d-block d-lg-none pl--20">
                                <span onClick={this.menuTrigger} className="menutrigger text-white"><FiMenu /></span>
                            </div>
                            {/* End Humberger Menu  */}
                            <div className="close-menu d-block d-lg-none">
                                <span onClick={this.CLoseMenuTrigger} className="closeTrigger"><FiX /></span>
                            </div>
                        </div>
                    </div>
                </header>
                {/* End Header Area   */}

                {/* Start Breadcrump Area */}
                <div className="rn-page-title-area ptb--50 bg_image bg_image--5"  data-black-overlay="5">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="rn-page-title text-center pt--100">
                                    <h2 className="title theme-gradient">Strony internetowe</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Breadcrump Area */}

                {/* Start Page Wrapper */}
                <div className="rn-service-details ptb--120 bg_color--1">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="service-details-inner">
                                    <div className="inner">
                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Strona internetowa wykorzystująca najnowsze technologie</h4>
                                                    <p>Nowoczesna strona internetowa powinna być zbudowana w oparciu o najnowsze technologie oraz spełniać najwyższe standardy Google i wytyczne W3C. Przestarzałe witryny nie tylko są nisko oceniane przez roboty pozycjonujące, ale i nie spełniają oczekiwań użytkowników. Dobra strona www to inwestycja, która pozwala wygrać z konkurencją i osiągnąć zamierzone cele. Tworząc nasze strony internetowe staramy się tak dobrać najnowsze technologie, aby były możliwie jak najlepiej dostosowane do indywidualnych potrzeb Twojej firmy. Uczciwie doradzamy lub odradzamy niektóre rozwiązania. Dzięki temu Twoja firmowa strona internetowa będzie odpowiedzią na Twoje oczekiwania i potrzeby Twojego biznesu.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Estetyczna i przejrzysta strona internetowa</h4>
                                                    <p>Wygląd ma znaczenie. Estetyczna strona internetowa świadczy o jej profesjonalizmie na równi z jej funkcjonalnością. Ciekawa i efektowna grafika pozwala skupić uwagę użytkownika, zatrzymać go dłużej na stronie i skłonić do podjęcia określonych działań. Dobra strona www powinna być również przejrzysta, tak aby nie przytłaczała ilością grafik i informacji. Tworzymy strony internetowe, których każdy element wizualny – od wyboru kolorów, poprzez krój czcionki po układ elementów – jest starannie przemyślany, a korzystanie z nich jest prawdziwą przyjemnością.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService2.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">W pełni funkcjonalna strona internetowa</h4>
                                                    <p>Funkcjonalne strony internetowe powinny oferować użytkownikowi te informacje, które go interesują w sposób łatwy i szybki. Intuicyjna nawigacja, optymalny formularz kontaktowy, działające odnośniki, mapka z lokalizacją firmy czy natychmiastowy dostęp do social mediów to tylko niektóre i absolutnie konieczne funkcjonalności, które decydują o skuteczności strony www. Funkcjonalna strona internetowa to sprawna strona, która szybko ładuję się oraz wyświetla się poprawnie na urządzeniach o różnej rozdzielczości. Dzięki takiej witrynie użytkownik nie opuści jej poirytowany, a Ty przestaniesz bezpowrotnie tracić swoich klientów.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Strona internetowa zoptymalizowana pod kątem użyteczności</h4>
                                                    <p>Nasza agencja interaktywna stawia nacisk, aby wszystkie zrealizowane przez nas strony internetowe były przyjazne użytkownikom. Dbamy o to, aby były one maksymalnie intuicyjne i łatwe w obsłudze. Właściwa struktura nawigacji, odpowiednie rozmieszczenie przycisków, działające linki oraz przejrzysta budowa strony sprawią, że użytkownik nie napotka żadnych problemów podczas poszukiwania interesujących go treści. Strony internetowe zoptymalizowane pod kątem użyteczności nie tylko poprawią ruch na Twojej stronie. Dodatkowo użytkownik będzie czerpała przyjemność z jej użytkowania i chętnie na nią powróci.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Responsywna strona internetowa</h4>
                                                    <p>Dobra, nowoczesna strona to przede wszystkim responsywna strona internetowa. Jej „elastyczna” budowa sprawia, że wyświetla się ona poprawnie na urządzeniach o różnej rozdzielczości – komputerach stacjonarnych, laptopach, tabletach oraz na smartfonach i iphone’ach. Dzięki temu Twój klient nie natrafi na problemy z „uciętą” treścią lub grafiką. Będzie mógł przeglądać ofertę Twojej firmy w drodze do pracy lub szkoły na telefonie lub tablecie. Z kolei Ty zyskasz przewagę nad konkurencją, która nie posiada responsywnej witryny i poszerzysz grono klientów. Dodatkowo responsywna strona www jest wyżej pozycjonowania przez Google oraz świadczy o profesjonalizmie Twojej firmy.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content align-items-center">
                                            <div className="col-lg-6 col-12 order-2 order-lg-1">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Strona internetowa z systemem zarządzania treścią CMS</h4>
                                                    <p>Współczesne witryny internetowe coraz bardziej odchodzą od projektowania stron statycznych, które do zarządzania wymagają znajomości programowania. Strony internetowe z wbudowanym systemem zarządzania CMS, to idealne rozwiązanie dla firm i osób fizycznych, ponieważ pozwalają na samodzielne dodawanie i publikowanie treści i grafiki. Intuicyjny i prosty w obsłudze panel zarządzania umożliwi Ci kontrolę nad stroną i nie wymaga specjalistycznej wiedzy. Specjalnie dla Ciebie zaprojektujemy autorski CMS lub wykorzystamy najpopularniejsze systemy zarządzania treścią stosowane przez strony internetowe największych światowych marek.</p>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12 order-1 order-lg-2">
                                                <div className="thumb position-relative">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}

                                        {/* Start Single Area */}
                                        <div className="row sercice-details-content pb--80 align-items-center">
                                            <div className="col-lg-6 col-12">
                                                <div className="thumb">
                                                    <img className="w-100" src="/assets/images/service/webPagesService1.png" alt="Service Images"/>
                                                </div>
                                            </div>
                                            <div className="col-lg-6 col-12">
                                                <div className="details mt_md--30 mt_sm--30">
                                                    <h4 className="title">Strona internetowa dostosowana do wyszukiwarek</h4>
                                                    <p>Projektujemy i budujemy strony internetowe zgodne z wytycznymi SEO. Logiczna budowa nawigacji, przyjazne linki oraz odnośniki wewnętrzne i zewnętrzne, odpowiednia struktura tytułów i nagłówków – te i wiele innych działań wdrażamy i przeprowadzamy w naszych realizacjach. Dzięki temu Twoja strona www już podczas fazy kreacji będzie osiągała jak najwyższe pozycje w naturalnych wynikach wyszukiwania. Dobra optymalizacja strony www pod wyszukiwarkę zwiększy zyski Twojej firmy poprzez nieustanny napływ nowych klientów.</p>
                                                </div>
                                            </div>
                                        </div>
                                        {/* End Single Area */}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Page Wrapper */}
                
                {/* Start Back To Top */}
                <div className="backto-top">
                    <ScrollToTop showUnder={160}>
                        <FiChevronUp />
                    </ScrollToTop>
                </div>
                {/* End Back To Top */}

                {/* Start Footer Style   */}
                <Footer />
                {/* End Footer Style  */}

            </React.Fragment>
        )
    }
}
export default WebPagesDetails;