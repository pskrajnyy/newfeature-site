import React, { Component } from "react";
import Tooltip from '@material-ui/core/Tooltip';
import Fade from '@material-ui/core/Fade';
import { withStyles } from "@material-ui/core/styles";

class Stack extends Component{
    render(){
        const stylesTooltip = {
            tooltip: {
                width: "auto",
                height: "36px",
                borderRadius: "18px",
                boxShadow: "0 20px 80px 0",
                fontSize: '20px',
              }
        };
    
        const CustomTooltip = withStyles(stylesTooltip)(Tooltip);

        return(
            <React.Fragment>
                <div className="container">
                    <div>
                        <div className="section-title text-left mb--30">
                            <h2>Narzędzia</h2>
                            <p>Wykorzystujemy sprawdzone technologie, by zapewnić naszym realizacjom wydajność i niezawodność. W razie potrzeby potrafimy szybko i skutecznie zaadoptować inne technologie lub stworzyć dedykowane rozwiązania. Nasze motto to "Nie ma rzeczy niemożliwych, wszystko jest kwestią czasu i pieniędzy".</p>
                        </div>
                    </div> 
                    <ul className="brand-style-2">           
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Wordpress">
                                <a href="https://pl.wordpress.org" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-01.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="PrestaShop">
                                <a href="https://www.prestashop.com/pl" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-02.png" alt="Logo Images"/>
                                </a>
                            </CustomTooltip>
                        </li>
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Bootstrap">
                                <a href="https://getbootstrap.com" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-08.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li> 
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="React.js">
                                <a href="https://pl.reactjs.org" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-03.png" alt="Logo Images"/>
                                </a>
                            </CustomTooltip>
                        </li>  
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Redux">
                                <a href="https://redux.js.org" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-21.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="AngularJS">
                                <a href="https://angular.io" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-04.png" alt="Logo Images"/>
                                </a>
                            </CustomTooltip>
                        </li>  
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Java">
                                <a href="https://www.java.com/pl/" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-06.png" alt="Logo Images"/>
                                </a>
                            </CustomTooltip>
                        </li> 
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="SpringBoot">
                                <a href="https://spring.io" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-05.png" alt="Logo Images"/>
                                </a>
                            </CustomTooltip>
                        </li>  
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="QraphQL">
                                <a href="https://graphql.org" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-07.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>    
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="IntelliJ IDEA">
                                <a href ="https://www.jetbrains.com/idea/" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-09.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>   
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Visual Studio Code">
                                <a href="https://code.visualstudio.com" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-10.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>   
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Docker">
                                <a href="https://www.docker.com" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-11.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>   
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Jira">
                                <a href="https://www.atlassian.com/pl/software/jira" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-12.png" alt="Logo Images"/>
                                </a>
                            </CustomTooltip>
                        </li>
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="GitLab">
                                <a href="https://gitlab.com" target="_blank" rel="noopener noreferrer">                                
                                    <img src="/assets/images/stack/stack-13.png" alt="Logo Images"/>
                                </a>
                            </CustomTooltip>
                        </li>  
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Jenkins">
                                <a href="https://www.jenkins.io" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-14.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Material UI">
                                <a href="https://material-ui.com" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-15.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Hibernate">
                                <a href="https://hibernate.org" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-16.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>   
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="nodeJS">
                                <a href="https://nodejs.org/en/" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-17.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>    
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="MySQL">
                                <a href="https://www.mysql.com" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-18.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>          
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="PostgreSQL">
                                <a href="https://www.postgresql.org" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-19.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>     
                        <li>
                            <CustomTooltip TransitionComponent={Fade} TransitionProps={{ timeout: 600 }} title="Sass">
                                <a href="https://sass-lang.com" target="_blank" rel="noopener noreferrer">
                                    <img src="/assets/images/stack/stack-20.png" alt="Logo Images"/>
                                </a>                            
                            </CustomTooltip>
                        </li>          
                    </ul>   
                </div>                
            </React.Fragment>
        )
    }
}
export default Stack;