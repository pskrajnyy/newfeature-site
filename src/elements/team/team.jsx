import React from 'react'
import { FaFacebookF , FaLinkedinIn , FaGithub, FaChrome } from "react-icons/fa";

const Team = (props) => {
    let TeamContent = [
        {
            images: 'przemek',
            title: 'Przemysław Skrajny',
            designation: 'Full-stack Developer',
            socialNetwork: [
                {
                    icon: <FaFacebookF />,
                    url: 'https://www.facebook.com/przemek.skrajny'
                },
                {
                    icon: <FaLinkedinIn />,
                    url: 'www.linkedin.com/in/przemysław-skrajny-b48309181'
                },
                {
                    icon: <FaGithub />,
                    url: 'https://gitlab.com/pskrajnyy'
                },
                {
                    icon: <FaChrome />,
                    url: 'https://przemyslawskrajny.pl'
                },
            ]
        }
    ];
    // const itemSlice = data.slice(0 , props.item)
    return (
        <div className="row">
            {TeamContent.map((value , i ) => (
                <div className={`${props.column}`} key={i}>
                    <div className={`team-static ${props.teamStyle}`}>
                        <div className="thumbnail">
                            <img src={`/assets/images/team/${value.images}.jpg`} alt="Blog Images"/>
                        </div>
                        <div className="inner">
                            <div className="content">
                                <h4 className="title">{value.title}</h4>
                                <p className="designation">{value.designation}</p>
                            </div>
                            <ul className="social-transparent liststyle d-flex" >
                                {value.socialNetwork.map((social, index) =>
                                    <li key={index}><a href={`${social.url}`}>{social.icon}</a></li>
                                )}
                            </ul>
                        </div>
                    </div>
                </div>
            ))}
        </div>
    )
}
export default Team;
