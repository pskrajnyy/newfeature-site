import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Switch, Route  } from 'react-router-dom';
import './index.scss';
import Home from './page/home/home';
import Error from './page/error/error';
import WebPages from './elements/service/serviceDetails/webPages/webPagesDetails';
import OnlineShops from './elements/service/serviceDetails/onlineShops/onlineShopsDetails.jsx';
import WebApps from './elements/service/serviceDetails/webApps/webAppsDetails.jsx';
import MobileApps from './elements/service/serviceDetails/mobileApps/mobileAppsDetails.jsx';

class Root extends Component{
    render(){
        return(
            <BrowserRouter basename={'/'}>
                <Switch>                  
                    <Route exact path={`${process.env.PUBLIC_URL}/`} component={Home}/>
                    <Route path={`${process.env.PUBLIC_URL}/404`} component={Error}/>
                    <Route path={`${process.env.PUBLIC_URL}/strony-internetowe`} component={WebPages}/>
                    <Route path={`${process.env.PUBLIC_URL}/sklepy-internetowe`} component={OnlineShops}/>
                    <Route path={`${process.env.PUBLIC_URL}/aplikacje-webowe`} component={WebApps}/>
                    <Route path={`${process.env.PUBLIC_URL}/aplikacje-mobilne`} component={MobileApps}/>
                    <Route component={Error}/>
                </Switch>
            </BrowserRouter>
        )
    }
}

ReactDOM.render(<Root/>, document.getElementById('root'));