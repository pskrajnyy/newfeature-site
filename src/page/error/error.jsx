import React, { Component } from 'react';
import ScrollToTop from 'react-scroll-up';
import Scrollspy from 'react-scrollspy';
import { FiChevronUp , FiX , FiMenu } from "react-icons/fi";
import Footer from "../../components/footer/footer";

 class Error extends Component {
    
    render() {
        return (
            <>         
            {/* Start Header Area   */}
            <header className="header-area formobile-menu header--fixed default-color">
                    <div className="header-wrapper" id="header-wrapper">
                        <div className="header-left">
                            <div className="logo">
                                <a href="/">
                                    <img className="logo-1" src="/assets/images/logo/logo.png" alt="Logo Images"/>
                                </a>
                            </div>
                        </div>
                        <div className="header-right">
                            <nav className="mainmenunav d-lg-block">
                                <Scrollspy className="mainmenu" items={['home','service', 'about', 'portfolio','team', 'contact']} currentClassName="is-current" offset={-200}>
                                    <li><a href="/#home">Home</a></li>
                                    <li><a href="/#service">Oferta</a></li>
                                    <li><a href="/#about">O nas</a></li>
                                    <li><a href="/#portfolio">Portfolio</a></li>
                                    <li><a href="/#team">Zespół</a></li>
                                    <li><a href="/#contact">Kontakt</a></li>
                                </Scrollspy>
                            </nav>
                            {/* Start Humberger Menu  */}
                            <div className="humberger-menu d-block d-lg-none pl--20">
                                <span onClick={this.menuTrigger} className="menutrigger text-white"><FiMenu /></span>
                            </div>
                            {/* End Humberger Menu  */}
                            <div className="close-menu d-block d-lg-none">
                                <span onClick={this.CLoseMenuTrigger} className="closeTrigger"><FiX /></span>
                            </div>
                        </div>
                    </div>
                </header>
                {/* End Header Area   */}   
                {/* Start Page Error  */}
                <div className="error-page-inner bg_color--4">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="inner">
                                    <h1 className="title theme-gradient">404!</h1>
                                    <h3 className="sub-title">Nie znalezionio strony</h3>
                                    <span>Wygląda na to że poszukiwana przez Ciebie strona nie istnieje</span>
                                    <div className="error-button">
                                        <a className="rn-button-style--2 btn-solid" href="/">Wróć na stronę główną</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* End Page Error  */}

                {/* Start Back To Top */}
                <div className="backto-top">
                    <ScrollToTop showUnder={160}>
                        <FiChevronUp />
                    </ScrollToTop>
                </div>
                {/* End Back To Top */}   

                {/* Start Footer */}
                <Footer />
                {/* End Footer */}        
            </>
        )
    }
}
export default Error;