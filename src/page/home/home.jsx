import React, { Component , Fragment } from "react";
import Particles from 'react-particles-js';
import ScrollToTop from 'react-scroll-up';
import Scrollspy from 'react-scrollspy';
import { FiChevronUp , FiX , FiMenu } from "react-icons/fi";
import Helmet from '../../components/helmet/helmet';
import Service from '../../elements/service/service';
import Portfolio from '../../components/portfolio/portfolio';
import Contact from '../../elements/contact/contact';
import Stack from '../../elements/stack/stack';
import Footer from '../../components/footer/footer';

const SlideList = [
    {
        textPosition: 'text-center',
        category: '',
        title: 'Software House New Feature ',
        description: 'Przedstaw nam swój problem, a my znajdziemy rozwiązanie',
        buttonText: 'Napisz do nas',
        buttonLink: '#contact'
    }
]

class Home extends Component{
    constructor(props) {
        super(props);
        this.menuTrigger = this.menuTrigger.bind(this);
        this.CLoseMenuTrigger = this.CLoseMenuTrigger.bind(this);
        this.stickyHeader = this.stickyHeader.bind(this);
    }

    menuTrigger() {
        document.querySelector('.header-wrapper').classList.toggle('menu-open')
    }

    CLoseMenuTrigger() {
        document.querySelector('.header-wrapper').classList.remove('menu-open')
    }

    stickyHeader () {}
    render(){        
        window.addEventListener('scroll', function() {
            var value = window.scrollY;
            if (value > 100) {
                document.querySelector('.header--fixed').classList.add('sticky')
            }else{
                document.querySelector('.header--fixed').classList.remove('sticky')
            }
        });

        var elements = document.querySelectorAll('.has-droupdown > a');
        for(var i in elements) {
            if(elements.hasOwnProperty(i)) {
                elements[i].onclick = function() {
                    this.parentElement.querySelector('.submenu').classList.toggle("active");
                    this.classList.toggle("open");
                }
            }
        }
        
        return(
            <Fragment>
                <Helmet pageTitle="NewFeature" />

                {/* Start Header Area   */}
                <header className="header-area formobile-menu header--fixed default-color">
                    <div className="header-wrapper" id="header-wrapper">
                        <div className="header-left" style={{width:'45%'}}>
                            <div className="logo">
                                <a href="/">
                                    <img className="logo-1" src="/assets/images/logo/logo.png" alt="Logo Images"/>
                                </a>
                            </div>
                        </div>
                        <div className="header-right">
                            <nav className="mainmenunav d-lg-block">
                                <Scrollspy className="mainmenu" items={['home','service', 'stack', 'about', 'portfolio', 'contact']} currentClassName="is-current" offset={-200}>
                                    <li><a href="#home" onClick={this.CLoseMenuTrigger}>Home</a></li>
                                    <li><a href="#service" onClick={this.CLoseMenuTrigger}>Oferta</a></li>
                                    <li><a href="#stack" onClick={this.CLoseMenuTrigger}>Narzędzia</a></li>
                                    <li><a href="#about" onClick={this.CLoseMenuTrigger}>O nas</a></li>
                                    <li><a href="#portfolio" onClick={this.CLoseMenuTrigger}>Portfolio</a></li>
                                    {/* <li><a href="#team" onClick={this.CLoseMenuTrigger}>Zespół</a></li> */}
                                    <li><a href="#contact" onClick={this.CLoseMenuTrigger}>Kontakt</a></li>
                                </Scrollspy>
                            </nav>
                            {/* Start Humberger Menu  */}
                            <div className="humberger-menu d-block d-lg-none pl--20">
                                <span onClick={this.menuTrigger} className="menutrigger text-white"><FiMenu /></span>
                            </div>
                            {/* End Humberger Menu  */}
                            <div className="close-menu d-block d-lg-none">
                                <span onClick={this.CLoseMenuTrigger} className="closeTrigger"><FiX /></span>
                            </div>
                        </div>
                    </div>
                </header>
                {/* End Header Area   */}

                {/* Start Slider Area */}
                
                <div className="slider-activation slider-creative-agency with-particles" id="home">
                    <div className="frame-layout__particles">
                        <Particles
                            params={{
                                "particles": {
                                    "number": {
                                        "value": 50
                                    },
                                    "size": {
                                        "value": 4
                                    }
                                },
                                "interactivity": {
                                    "events": {
                                        "onhover": {
                                            "enable": true,
                                            "mode": "repulse"
                                        }
                                    }
                                }
                            }}
                        />
                    </div>
                    <div className="bg_image bg_image--1">
                            {SlideList.map((value , index) => (
                                <div className="slide slide-style-2 slider-paralax d-flex align-items-center justify-content-center" key={index}>
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-lg-12">
                                                <div className={`inner ${value.textPosition}`}>
                                                    {value.category ? <span>{value.category}</span> : ''}
                                                    {value.title ? <h1 className="title theme-gradient">{value.title}</h1> : ''}
                                                    {value.description ? <p className="description">{value.description}</p> : ''}
                                                    {value.buttonText ? <div className="slide-btn"><a className="rn-button-style--2 btn-primary-color" href={`${value.buttonLink}`}>{value.buttonText}</a></div> : ''}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                </div>
                {/* End Slider Area */}

                {/* Start Service Area  */}
                <div className='active-dark' id='service'>
                    <div className="service-area ptb--80  bg_image bg_image--3">
                        <div className="container">
                                <Service />
                        </div>
                    </div>
                </div>                
                {/* End Service Area  */}

                {/* Start Brand Area */}
                <div className="rn-brand-area brand-separation bg_color--5 ptb--120" id="stack">                    
                    <div className="row">
                        <div className="col-lg-12">
                            <Stack />
                        </div>
                    </div>
                </div>
                {/* End Brand Area */}

                {/* Start About Area */}
                <div className='active-dark'>
                    <div className="about-area ptb--120 bg_color--1" id="about">
                        <div className="about-wrapper">
                            <div className="container">
                                <div className="row row--35 align-items-center">
                                    <div className="col-lg-5">
                                        <div className="thumbnail">
                                            <img className="w-100" src="/assets/images/about/about.jpg" alt="About Images"/>
                                        </div>
                                    </div>
                                    <div className="col-lg-7">
                                        <div className="about-inner inner">
                                            <div className="section-title">
                                                <h2 className="title">O nas</h2>
                                                <p className="description"><b>New Feature</b> tworzą młodzi ludzie, którzy kochają to, co robią, dlatego wiemy, że najlepsze projekty rodzą się z pasji. Dbamy o to, aby wszystkie nasze realizacje były maksymalnie nowoczesne i funkcjonalne, a obserwowanie jak wzrasta Twój biznes, sprawia nam ogromną satysfakcję. Naszą ambicją jest złożenie w Twoje ręce projektów, z których możemy być dumni.</p>
                                            </div>
                                            <div className="row mt--30">
                                                <div className="col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div className="about-us-list">
                                                        <h3 className="title">Nasze zasady</h3>
                                                        <p><b>Kod musi być prosty, przejrzysty, udokumentowany</b></p>
                                                        <p><b>Obsługa, która sprawi, że zostaniesz z nami na długo</b></p>
                                                        <p><b>Doskonała komunikacja na każdym etapie procesu realizacji</b></p>
                                                        <p><b>Rozumiemy oczekiwania Klienta i dowozimy satysfakcję</b></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>                
                {/* End About Area */}

                {/* Start Portfolio Area */}
                <div className='active-dark'>
                <div className="portfolio-area ptb--120 bg_image bg_image--3" id="portfolio"> 
                        <div className="portfolio-sacousel-inner mb--55">
                            <Portfolio />
                        </div>
                    </div>
                </div>                
                {/* End Portfolio Area */}

                {/* Start Team Area  */}
                {/* <div className='active-dark'>
                    <div className="rn-team-area ptb--120 bg_color--1" id="team">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-9">
                                    <div className="section-title service-style--3 text-left mb--25 mb_sm--0">
                                        <h2 className="title">Nasz zespół</h2>
                                        <p>Siłą NewFeature są ludzie pełni pasji. Każdy z nas ma swoje zainteresowania, każdy przynosi do
                                            pracy pewną energię, która przekłada się na atmosferę w realizowanych projektach. Jedni stawiają
                                            na sport i adrenalinę, inni szukają spokoju w medytacji, ale każdy czuje się tu jak w domu.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div className="row teamImages">
                                <Team teamStyle="ourTeam"/>
                            </div>
                        </div>
                    </div>
                </div>                 */}
                {/* End Team Area  */}

                {/* Start Contact Us */}
                <div className='active-dark'>
                    <div className="rn-contact-us ptb--120 bg_color--5" id="contact">
                        <Contact />
                    </div>
                </div>                    
                {/* End Contact Us */}

                {/* Start Footer Style   */}
                <Footer />
                {/* End Footer Style  */}

                {/* Start Back To Top  */}
                <div className="backto-top">
                    <ScrollToTop showUnder={160}>
                        <FiChevronUp />
                    </ScrollToTop>
                </div>
                {/* End Back To Top  */}
            </Fragment>
        )
    }
}
export default Home;